<?php
require_once 'vendor\autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use app\model\carte;
use app\controller\GameController;
use \app\view\VueAccueil;
use \app\view\VueChoixSalon;
use app\controller\ConnexionController;
use \app\controller\RoomController;
use \app\controller\CardController;
use \app\view\VueInscription;
use app\controller\InscriptionController;
use app\model\faceVisible;
use app\model\joueur;
use app\model\joueurMain;
use app\model\joueurDefausse;
use app\model\historique;

//initialisation
session_start();

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();



//ROUTER
$app->get('/', function() {
    $accueil = new VueAccueil();
    $accueil->render(1);
})->name("login");

$app->get('/login/fail',function (){
    $accueil = new VueAccueil();
    $accueil->render(2);
})->name("loginFail");

$app->get('/game/:id', function ($id){
    $cartes = new GameController();
    $cartes->rejoindreSalon($id);
})->name("game");

$app->post('/connexion', function(){
    $res = ConnexionController::verifierConnexion();
    $app = \Slim\Slim::getInstance();
    if($res == true) {
        $app->response->redirect($app->urlFor("games"));
    }else{
        $app->response->redirect($app->urlFor("loginFail"));
    }
})->name("verifierConnexion");

$app->get('/games',function (){
    $salon = new VueChoixSalon();
    $salon->render();
})->name("games");

$app->post('/games/add', function(){
    $r = RoomController::creerSalon();
    $app = \Slim\Slim::getInstance();
    $app->response->redirect($app->urlFor("games"));
})->name("gamesAdd");

$app->get('/draw', function ($id){
    $gc = new GameController();
    $gc->piocher($id);
    //$carte =
    $gc->rejoindreSalon($id);
    //return $carte;
})->name("draw");

$app->get("/inscription", function (){
    $ins = new VueInscription();
    $ins->render(1);
})->name("inscription");

$app->get("/inscription/fail", function(){
    $ins = new VueInscription();
    $ins->render(2);
})->name("inscription2");

$app->post("/inscription/verifier", function (){
    $app = \Slim\Slim::getInstance();
    $res = new InscriptionController();
    $r = $res->verifierInscription();
    if($r == true){
        $app->response->redirect($app->urlFor("login"));
    }else{
        $app->response->redirect($app->urlFor("inscription2"));
    }

})->name("validerInscription");




$app->post('/choix/:n', function ($num){

    $app = \Slim\Slim::getInstance();
    $joueur = joueur::find($_SESSION["idJoueur"]);
    //$app->response->redirect($app->urlFor("login"));
    if($joueur == null){
        echo "erreur";
    }else{

        $gc = new GameController();
        echo $gc->choix($num);
    }

})->name("choix");

$app->post('/jouer/:n', function ($n){

    $app = \Slim\Slim::getInstance();
    $joueur = joueur::find($_SESSION["idJoueur"]);
    //$app->response->redirect($app->urlFor("login"));
    if($joueur == null){
        echo "erreur";
    }else{
        if($joueur == null){
            echo "erreur";
        }else{

            $gc = new GameController();
            $res = $gc->jouer($joueur['id'],$n);
            echo $res;
        }
    }

})->name("jouer");

$app->post('/affichage', function (){
    $aff = new GameController();
    $aff->affichage();
})->name("affichage");

//run
$app->run();