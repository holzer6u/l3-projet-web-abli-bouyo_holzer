<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 07/12/2017
 * Time: 18:58
 */

namespace app\controller;
use app\model\joueur;

class InscriptionController
{

    public function verifierInscription(){
        $j = joueur::where('login',$_POST["pseudo"])->first();
        if(empty($j)){
            $joueur = new joueur();
            $joueur->login = $_POST['pseudo'];
            $joueur->mdp = $_POST["mdp"];
            $joueur->score = 0;
            $joueur->save();
            return true;
        }else{
            return false;
        }
    }

}