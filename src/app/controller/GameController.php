<?php
namespace app\controller;
use app\model\carte;
use app\model\historique;
use app\model\joueurDefausse;
use app\model\partie;
use app\model\deck;
use app\model\faceVisible;
use app\model\joueur;
use app\model\joueurMain;
use app\view\VueCartes;
use app\model\manche;

class GameController
{

    public static function afficherCartes()
    {
        $vue = new VueCartes("");
        $idDeckCree = self::creerDeck();
        $listeCartes = null;
        if ($_SESSION['nbJoueur'] == 2) {
            $listeCartes = self::bruler3Cartes($idDeckCree);
        }
        $listeJoueurs = self::distribuerLesCartes();
        echo $vue->render();
    }

    public function rejoindreSalon($id)
    {
        $i = 0;
        $vue = new VueCartes("");
        $partie = partie::where("id", $id)->first();
        $partieNbJoueur = $partie["nbJoueur"];
        $partieIdManche = $partie["idManche"];
        $_SESSION["id"] = $id;
        $_SESSION["nbJoueur"] = $partieNbJoueur;
        $_SESSION["idManche"] = $partieIdManche;

        $joueur = joueur::find($_SESSION["idJoueur"]);
        $joueur->idManche = $partieIdManche;
        $joueur->save();

        $joueursDansPartie = joueur::where("idManche", $_SESSION["idManche"])->get();
        foreach ($joueursDansPartie as $j) {
            $i++;
        }

        $manche = manche::where("idManche", $_SESSION["idManche"])->first();
        $mancheEnCours = $manche["enCours"];

        if ($i == $partieNbJoueur && $mancheEnCours == 0) {
            self::afficherCartes();
        } else if ($mancheEnCours == 1) {
            //$res = $this->piocher($_SESSION["idJoueur"]);
            echo $vue->render();
            //return $res;
        } else {
            echo $vue->render();
        }

    }


    public static function creerDeck()
    {
        $nbJ = $_SESSION["nbJoueur"];
        $idManche = $_SESSION['idManche'];
        $tab = carte::select('idCarte')->get()->toArray();
        for ($i = 0; $i < 16; $i++) {
            $alea = rand(1, count($tab));
            $deck[$i] = $tab[$alea - 1];
            unset($tab[$alea - 1]);
            $tab = array_values($tab);
        }
        $idDeck = deck::all()->groupBy('idDeck')->count('DISTINCT idDeck') + 1;
        $_SESSION["idDeck"] = $idDeck;
        echo "<br>";
        foreach ($deck as $d) {
            //deck::create(['idDeck'=>$idDeck], ['idCarte'=>$d]);
            $deckBdd = new deck();
            $deckBdd->idDeck = $idDeck;
            $deckBdd->idCarte = $d['idCarte'];
            $deckBdd->idManche = $idManche;
            $deckBdd->save();
        }
        $manche = manche::find($idManche)->first();
        $manche->idDeck = $idDeck;
        $manche->save();
        return $idDeck;
    }

    public static function bruler3Cartes($idDeck)
    {
        $ii = 0;
        $idManche = $_SESSION['idManche'];

        $cartesBrulees = deck::orderByRaw("RAND()")->take(3)->where('idDeck', $idDeck)->get();


        $log = "Trois cartes ont été brulées: ";

        foreach ($cartesBrulees as $cB) {
            $c = carte::where("idCarte", $cB['idCarte'])->first();
            $listeCartesBrulees[$ii] = $c['nom'];
            $log .= $c['nom'] . " ";

            $faceVisible = new faceVisible();
            $faceVisible->idCarte = $cB['idCarte'];
            $faceVisible->idManche = $idManche;
            $faceVisible->save();
            $ii++;

            deck::where('idCarte', $faceVisible->idCarte)->where("idManche", $idManche)->delete();


        }

        $historique = new historique();
        $historique->idManche = $idManche;
        $historique->contenu = $log;
        $historique->save();

        return $listeCartesBrulees;
    }

    public static function distribuerLesCartes()
    {
        $idManche = $_SESSION['idManche'];
        $listeJoueurs = null;
        $joueurs = joueur::where('idManche', $idManche)->get();
        $ii = 0;
        $order = 1;
        $idCommence = 0;
        foreach ($joueurs as $j) {

            $carte = deck::orderByRaw("RAND()")->where('idManche', $idManche)->first();
            $idC = $carte->idCarte;
            $jId = $j['id'];
            $listeJoueurs[$ii] = $jId;
            $ii++;

            $j->ordre = $order;
            $j->protege = 0;
            $j->save();
            if ($order == 1) {
                $historique = new historique();
                $historique->idManche = $idManche;
                $historique->contenu = $j->login . " commence.";
                $historique->save();
                $idCommence = $jId;
            }
            $order++;

            $main = new joueurMain();
            $main->idJoueur = $j['id'];
            $main->idCarte = $idC;
            $main->save();

            deck::where('idCarte', $idC)->where('idManche', $idManche)->delete();
        }
        $manche = manche::find($idManche)->first();
        $manche->enCours = 1;
        $manche->save();

        self::piocher($idCommence);

        return $listeJoueurs;
    }

    public static function recupererCartesJoueurs($listeJoueur)
    {
        $lc = null;
        $i = 0;
        $ii = 0;
        foreach ($listeJoueur as $joueur) {
            $listeCartesJoueur = joueurMain::where('idJoueur', $joueur)->get();
            foreach ($listeCartesJoueur as $carte) {
                $lc[$i] = [$ii => $carte["nom"]];
                $ii++;
            }
            $i++;
        }
        return $lc;
    }

    public function piocher($idJ)
    {
        $res = true;

        $idManche = $_SESSION['idManche'];
        $carte = deck::orderByRaw("RAND()")->where('idManche', $idManche)->first();
        if($carte != NULL){
            $idCarte1 = $carte['idCarte'];

            $joueurMain = new joueurMain();
            $joueurMain->idJoueur = $idJ;
            $joueurMain->idCarte = $idCarte1;
            $joueurMain->save();

            deck::where("idManche", $_SESSION["idManche"])->where("idCarte", $idCarte1)->delete();

            //$carteImg = carte::where('idCarte', $idCarte1)->first();
            //$carteImg = $carteImg["img"];

            $res = false;
        }
        return $res;

    }

    public function eliminer($idJ)
    {
        $perdant = joueur::where('id',$idJ)->first();
        $perdant->ordre = 0;
        $perdant->save();

        $carte1 = joueurMain::where('idJoueur', $perdant['id'])->first();
        $cc1 = $carte1['idCarte'];
        $a = $perdant['id'];
        joueurMain::where('idJoueur', $a)->where('idCarte', $cc1)->delete();

        $joueurDefausse = new joueurDefausse();
        $joueurDefausse->idJoueur = $a;
        $joueurDefausse->idCarte = $cc1;
        $joueurDefausse->save();

    }


    public function choix($num){

        $finirTour = true;
        $res = "okChoix";

        switch ($_SESSION['choix']) {
            case "Baron":
                $jActif = joueur::where('id',$_SESSION["idJoueur"])->first();
                $cible = joueur::where('id',$num)->first();


                $carte1 = joueurMain::where('idJoueur', $jActif['id'])->first();
                $carte2 = joueurMain::where('idJoueur', $cible['id'])->first();

                $cc1 = $carte1['idCarte'];
                $cc2 = $carte2['idCarte'];

                $vc1 = carte::where('idCarte', $cc1)->first();
                $vc2 = carte::where('idCarte', $cc2)->first();

                if($vc1['valeur'] < $vc2['valeur']){
                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login'] . " joue un Baron en ciblant " . $cible['login'] . ". Sa carte est plus petite que celle de " . $cible['login'] .". ".$jActif['login'] ." perd la manche!";
                    $historique->save();

                    self::eliminer($jActif['id']);
                    /*$jActif->ordre = 0;
                    $jActif->save();*/

                }else{
                    if($vc1['valeur'] > $vc2['valeur']){
                        $historique = new historique();
                        $historique->idManche = $_SESSION["idManche"];
                        $historique->contenu = $jActif['login'] . " joue un Baron en ciblant " . $cible['login'] . ". Sa carte est plus grande que celle de " . $cible['login'] .". ".$cible['login'] ." perd la manche!";
                        $historique->save();

                        self::eliminer($cible['id']);
                        /*$cible->ordre = 0;
                        $cible->save();*/

                    }else{
                        $historique = new historique();
                        $historique->idManche = $_SESSION["idManche"];
                        $historique->contenu = $jActif['login'] . " joue un Baron en ciblant " . $cible['login'] . ". Ils ont la même carte !";
                        $historique->save();

                    }
                }



                break;
            case "Priest":

                $jActif = joueur::where('id',$_SESSION["idJoueur"])->first();
                $cible = joueur::where('id',$num)->first();
                $historique = new historique();
                $historique->idManche = $_SESSION["idManche"];
                $historique->contenu = $jActif['login'] . " joue un Priest en ciblant " . $cible['login'] . ".";
                $historique->save();

                $cartes = joueurMain::where('idJoueur',$cible['id'])->get();
                $res = "revelerMain|".$cible['login']."|";

                $app = \Slim\Slim::getInstance();
                $img = $app->urlFor('login');

                foreach ($cartes as $c) {
                    $carte = carte::select('img')->where('idCarte', $c['idCarte'])->first();
                    $lienImg = $carte['img'];
                    $res .= $img . "/web/".$lienImg."|";

                }
                $res.="finRev|";
                break;
            case "Guard":

                $_SESSION['choix'] = "GuardEtape2";
                $_SESSION['choixGuard'] = $num;
                $res = "choixCarte|";
                $finirTour = false;
            break;
            case "GuardEtape2":

                $jActif = joueur::where('id',$_SESSION["idJoueur"])->first();
                $cible = joueur::where('id',$_SESSION['choixGuard'])->first();
                $cMain = joueurMain::where('idJoueur',$cible['id'])->first();
                $carte = carte::where('idCarte', $cMain['idCarte'])->first();
                $carteNommee = carte::where('valeur', $num)->first();

                if($num == $carte['valeur']){

                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login']." joue un Guard en ciblant ".$cible['login']." et en nommant le ". $carteNommee['nom'].". Il devine juste! ".$cible['login']." perd la manche.";
                    $historique->save();

                    self::eliminer($cible['id']);
                    /*$cible->ordre = 0;
                    $cible->save();*/

                }else{
                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login']." joue un Guard en ciblant ".$cible['login']." et en nommant le ".$carteNommee['nom'] .". Mais il se trompe!";
                    $historique->save();
                }


                break;
            case "Prince":

                $jActif = joueur::where('id',$_SESSION["idJoueur"])->first();
                $cible = joueur::where('id',$num)->first();
                $historique = new historique();
                $historique->idManche = $_SESSION["idManche"];
                $historique->contenu = $jActif['login'] . " joue un Prince en ciblant " . $cible['login'] . ". " . $cible['login'] . " défausse sa carte et pioche.";
                $historique->save();

                $carte1 = joueurMain::where('idJoueur', $cible['id'])->first();
                $cc1 = $carte1['idCarte'];
                $a = $cible['id'];
                joueurMain::where('idJoueur', $a)->where('idCarte', $cc1)->delete();

                $joueurDefausse = new joueurDefausse();
                $joueurDefausse->idJoueur = $a;
                $joueurDefausse->idCarte = $cc1;
                $joueurDefausse->save();

                self::piocher($a);

                if($cc1 == 16){

                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $cible['login']. " défausse la Princess est perd la manche!";
                    $historique->save();

                    self::eliminer($cible['id']);
                    /*$cible->ordre = 0;
                    $cible->save();*/

                }

                break;
            case "King":

                $jActif = joueur::where('id',$_SESSION["idJoueur"])->first();
                $cible = joueur::where('id',$num)->first();
                $historique = new historique();
                $historique->idManche = $_SESSION["idManche"];
                $historique->contenu = $jActif['login'] . " joue le King et échange sa main avec " . $cible['login'] . ".";
                $historique->save();

                $carte1 = joueurMain::where('idJoueur', $jActif['id'])->first();
                $carte2 = joueurMain::where('idJoueur', $cible['id'])->first();

                $cc1 = $carte1['idCarte'];
                $cc2 = $carte2['idCarte'];

                $a = $jActif['id'];
                $b = $cible['id'];

                joueurMain::where('idJoueur', $a)->where('idCarte', $cc1)->delete();
                joueurMain::where('idJoueur', $b)->where('idCarte', $cc2)->delete();

                $carte1 = new joueurMain();
                $carte1->idJoueur = $cible['id'];
                $carte1->idCarte = $cc1;
                $carte1->save();

                //joueurMain::where('idJoueur',$cible['id'])->first();
                $carte2 = new joueurMain();
                $carte2->idJoueur = $jActif['id'];
                $carte2->idCarte = $cc2;
                $carte2->save();

                break;

        }

        if($finirTour ){
            if(!self::finTour()){
                $res = "GAME FINIE";
            };
        }

        return $res;
    }

    public function jouer($idJ, $num)
    {
        $res = "ok";


        $joueurDefausse = new joueurDefausse();
        $joueurDefausse->idJoueur = $idJ;
        $joueurDefausse->idCarte = $num;
        $joueurDefausse->save();

        joueurMain::where('idJoueur', $idJ)->where('idCarte', $num)->delete();


        $jActif = joueur::where('id', $idJ)->first();

        $finirTour = true;

        switch ($num) {
            //GUARD
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                $joueursSel = joueur::where("idManche", $_SESSION["idManche"])->where("id", '!=', $idJ)->where("protege", 0)->where("ordre", '!=', 0)->get();
                if ($joueursSel->first() != NULL) {
                    $res = "selection|";
                    foreach ($joueursSel as $js) {

                        $res .= $js['id'] . "|" . $js['login'] . "|";

                    }
                    $res .= "finSel|";

                    $_SESSION['choix'] = "Guard";
                    $finirTour = false;
                } else {
                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login'] . " joue un Guard.";
                    $historique->save();
                }

                break;

            //PRIEST
            case 6:
            case 7:


                $joueursSel = joueur::where("idManche", $_SESSION["idManche"])->where("id", '!=', $idJ)->where("protege", 0)->where("ordre", '!=', 0)->get();
                if ($joueursSel->first() != NULL) {
                    $res = "selection|";
                    foreach ($joueursSel as $js) {

                        $res .= $js['id'] . "|" . $js['login'] . "|";

                    }
                    $res .= "finSel|";

                    $_SESSION['choix'] = "Priest";
                    $finirTour = false;
                } else {
                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login'] . " joue un Priest.";
                    $historique->save();
                }


                break;

            //BARON
            case 8:
            case 9:
                $joueursSel = joueur::where("idManche", $_SESSION["idManche"])->where("id", '!=', $idJ)->where("protege", 0)->where("ordre", '!=', 0)->get();
                if ($joueursSel->first() != NULL) {
                    $res = "selection|";
                    foreach ($joueursSel as $js) {

                        $res .= $js['id'] . "|" . $js['login'] . "|";

                    }
                    $res .= "finSel|";

                    $_SESSION['choix'] = "Baron";
                    $finirTour = false;
                } else {
                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login'] . " joue un Baron.";
                    $historique->save();
                }

                break;

            //HANDMAID
            case 10:
            case 11:

                $historique = new historique();
                $historique->idManche = $_SESSION["idManche"];
                $historique->contenu = $jActif['login'] . " joue une Handmaid. Il est protégé jusqu'à son prochain tour.";
                $historique->save();

                $jActif->protege = 1;
                $jActif->save();

                break;

            //PRINCE
            case 12:
            case 13:
                $joueursSel = joueur::where("idManche", $_SESSION["idManche"])->where("protege", 0)->where("ordre", '!=', 0)->get();
                if ($joueursSel->first() != NULL) {
                    $res = "selection|";
                    foreach ($joueursSel as $js) {

                        $res .= $js['id'] . "|" . $js['login'] . "|";

                    }
                    $res .= "finSel|";

                    $_SESSION['choix'] = "Prince";
                    $finirTour = false;
                } else {
                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login'] . " joue un Prince.";
                    $historique->save();

                }
                break;
            //KING
            case 14:
                $joueursSel = joueur::where("idManche", $_SESSION["idManche"])->where("id", '!=', $idJ)->where("protege", 0)->where("ordre", '!=', 0)->get();
                if ($joueursSel->first() != NULL) {
                    $res = "selection|";
                    foreach ($joueursSel as $js) {

                        $res .= $js['id'] . "|" . $js['login'] . "|";

                    }
                    $res .= "finSel|";

                    $_SESSION['choix'] = "King";
                    $finirTour = false;
                } else {
                    $historique = new historique();
                    $historique->idManche = $_SESSION["idManche"];
                    $historique->contenu = $jActif['login'] . " joue le King.";
                    $historique->save();
                    break;
                }

            //COUNTESS
            case 15:

                $historique = new historique();
                $historique->idManche = $_SESSION["idManche"];
                $historique->contenu = $jActif['login'] . " joue la Countess.";
                $historique->save();
                break;

            //PRINCESS
            case 16:

                $historique = new historique();
                $historique->idManche = $_SESSION["idManche"];
                $historique->contenu = $jActif['login'] . " joue la Princess. Il perd la manche.";
                $historique->save();

                self::eliminer($jActif['id']);
                /*$jActif->ordre = 0;
                $jActif->save();*/

                break;
        }

        if($finirTour ){
            if(!self::finTour()){
                $res = "GAME FINIE";
            };
        }


        return $res;
    }

    public function finTour()
    {
        $res = true;
        $nbEnVie = 0;
        $idDernier = 0;
        $idProchain = 0;

        $joueurs = joueur::where('idManche', $_SESSION["idManche"])->get();


        foreach ($joueurs as $j) {
            if ($j['ordre'] != 0) {
                $nbEnVie++;
                $jEnVie = $j;
                if ($j['ordre'] == 1) {
                    $idDernier = $j['id'];
                }
                if ($j['ordre'] == 2) {
                    $idProchain = $j['id'];
                    $j->protege = 0;
                }
                $j->ordre = $j['ordre'] - 1;
                $j->save();
            }

        }

        if ($idDernier != 0) {
            $jj = joueur::where('id', $idDernier)->first();
            $jj->ordre = $nbEnVie;
            $jj->save();
        }
        $deckVide = false;

        if($nbEnVie>1){
            $deckVide = self::piocher($idProchain);
        }


        if($nbEnVie<=1 || $deckVide){
            $res = false;

            $historique = new historique();
            $historique->idManche = $_SESSION["idManche"];
            $historique->contenu = "La manche est terminée! " .$jEnVie['login']." gagne un point.";
            $historique->save();
            $jEnVie = joueur::where('idManche', $_SESSION["idManche"])->where('ordre','!=', 0)->first();
            self::eliminer($jEnVie['id']);
            /*$jEnVie->ordre = 0;
            $jEnVie->save();*/
        }

        return $res;
    }


    public function affichage(){
        $app = \Slim\Slim::getInstance();
        $j = joueur::find($_SESSION["idJoueur"]);
        $joueurs = joueur::where('idManche',$_SESSION['idManche'])->get();
        $img = $app->urlFor('login');
        if($j == null){
            echo "erreur";
        }else {
            $res = "";

            /*$carteJoueurMain = joueurMain::where('idJoueur', $joueur['id'])->get();
            $joueurId = $joueur['id'];*/

            foreach ($joueurs as $joueur){
                $joueurId = $joueur['id'];
                //$joueurNom = joueur::where('id',$joueurId)->first();
                //$joueurNom = $joueurNom['login'];
                if ($joueurId == $_SESSION['idJoueur']) {
                    $res .="CTOI|";
                    if ($joueur['ordre'] == 1) {

                        $countess = joueurMain::where('idJoueur',$joueur['id'])->where('idCarte', "15")->first();
                        $king = joueurMain::where('idJoueur',$joueur['id'])->where('idCarte', "14")->first();
                        $prince = joueurMain::where('idJoueur',$joueur['id'])->where('idCarte', "13")->first();

                        if(($countess != NULL && $king != NULL) || ($countess != NULL && $prince != NULL)){
                            $res .="CTONTOURmaisCountess|";
                        }else{
                            $res .="CTONTOUR|";
                        }


                    }
                }
                $carteJoueurMain = joueurMain::where('idJoueur',$joueur['id'])->get();
                foreach ($carteJoueurMain as $cJ) {
                    $carte = carte::where('idCarte', $cJ['idCarte'])->first();
                    //$nomImg = $carte['nom'];
                    if ($joueurId == $_SESSION['idJoueur']) {
                        $lienImg = $carte['img'];
                        // $idJoueur
                        $res .= $carte['idCarte']."|". $img . "/web/" . $lienImg . "|";
                    } else {
                        $res .= $img . "/web/img/dos.png|";
                    }
                }
                $res.="finJ|";
            }
            $res.="finCM|";
            $cartesBrulees = faceVisible::where('idManche', $_SESSION['idManche'])->get();
            foreach ($cartesBrulees as $cb) {

                $idCarte = $cb['idCarte'];
                $carte = carte::select('nom', 'img')->where('idCarte', $idCarte)->first();
                //$nomCarte = $carte['nom'];
                $lienImg = $carte['img'];
                $res.=$img . "/web/". $lienImg."|";

            }
            $res.="finCB|";

            foreach ($joueurs as $joueur) {
                $joueurId = $joueur['id'];
                //$joueurNom = joueur::where('id', $joueurId)->first();
                //$joueurNom = $joueurNom['login'];
                $carteJoueurDefausse = joueurDefausse::where('idJoueur', $joueur['id'])->get();

                foreach ($carteJoueurDefausse as $cJ) {
                    $carte = carte::where('idCarte', $cJ['idCarte'])->first();
                    $lienImg = $carte['img'];
                    $res.=$img . "/web/".$lienImg."|";
                }
                $res.="finJD|";
            }
            $res.="finCD|";

            $historique = historique::where('idManche', $_SESSION['idManche'])->get();
            foreach ($historique as $h) {

                $idH = $h['idItem'];
                $log = historique::select('contenu')->where('idItem', $idH)->first();
                $res.= $log['contenu']."|";

            }
            $res.="finH|";

            echo $res;
        }
    }

}
/*
//Créer un nouveau deck
tab = {1,2,3,...,15}
pour I de 1 à 15 faire

    R <= rnd(1 , tab.length)
    insert into Deck values(B, tab[R]);
    supprimer tab[R]

fpour

// Bruler 3 cartes
si (select nbJoueurs from Partie where idPartie = A) ==2 alors
    C <= select idCarte from Deck where idManche = B limit 3;
    pour chaque tuple T de C faire
        insert into FaceVisible values (B, T.idCarte)
        Delete from Deck where idManche = B and idCarte = T.idCarte
    fpour
fsi

// Distribuer les cartes
D <= select * from Joueur where idManche = B;
pour chaque tuple T de D faire
    insert into Main values (B, T.idCarte)
    Delete from Deck where idManche = B and idCarte = T.idCarte
fpour
 */

