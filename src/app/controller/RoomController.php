<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 07/12/2017
 * Time: 11:35
 */

namespace app\controller;
use app\model\partie;
use app\model\manche;


class RoomController
{

    public static function creerSalon(){
        $nbJoueur = $_POST["nbJ"];

        $manche = new manche();
        $manche->save();

        $salon = new partie();
        $salon->nbJoueur = $nbJoueur;
        $salon->idManche = $manche->idManche;
        $salon->save();

    }

}