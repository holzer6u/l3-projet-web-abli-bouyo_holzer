<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 07/12/2017
 * Time: 14:08
 */

namespace app\controller;
use app\model\carte;
use app\model\joueurMain;

class CardController
{

    public function garde($idJ,$idC){
        $carte = joueurMain::where('idJoueur',$idJ)->where('idCarte',$idC)->first();
        if(!empty($carte)){
            return true;
        }
        return false;
    }

    public function pretre($idJ){
        $carte = joueurMain::where('idJoueur',$idJ)->first();
        $carteId = $carte['idCarte'];
        return $carteId;
    }

    public function baron($idJ1,$idJ2){
        $carteJ1 = joueurMain::where('idJoueur',$idJ1)->first();
        $carteJ2 = joueurMain::where('idJoueur',$idJ2)->first();

        $carteJ1 = carte::where('idCarte',$carteJ1['idCarte'])->first();
        $carteJ1val = $carteJ1['valeur'];

        $carteJ2 = carte::where('idCarte',$carteJ2['idCarte'])->first();
        $carteJ2val = $carteJ2['valeur'];

        if($carteJ1val > $carteJ2val){
            return $idJ2;
        }else if($carteJ1val < $carteJ2val){
            return $idJ1;
        }else{
            return null;
        }
    }

    public function servante(){

    }

    public function prince(){

    }

    public function roi(){

    }

    public function contesse(){

    }

    public function princesse(){

    }

}