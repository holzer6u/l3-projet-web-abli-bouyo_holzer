<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 03/12/2017
 * Time: 13:09
 */

namespace app\controller;
use app\model\joueur;

class ConnexionController
{

    public function verifierConnexion(){
        $res = false;
        $j = joueur::where('login',$_POST["pseudo"])->where('mdp',$_POST["mdp"])->first();
        if($j){
            $res = true;
            if(isset($_SESSION["idJoueur"])){
                unset($_SESSION["idJoueur"]);
                $_SESSION["idJoueur"] = $j["id"];
            }else{
                $_SESSION["idJoueur"] = $j["id"];
            }
        }

        return $res;
    }

}