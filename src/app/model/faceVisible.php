<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:42
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class faceVisible extends Model
{
    protected $table = 'facevisible';
    protected $primaryKey = "idCarte,idManche";
    public $timestamps = false;
}