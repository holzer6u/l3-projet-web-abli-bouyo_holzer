<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:43
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class joueur extends Model
{
    protected $table = 'joueur';
    protected $primaryKey = "id";
    public $timestamps = false;
}