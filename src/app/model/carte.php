<?php
namespace app\model;
use Illuminate\Database\Eloquent\Model;

class carte extends Model
{
    protected $table = 'carte';
    protected $primaryKey = "idCarte";
    public $timestamps = false;
}