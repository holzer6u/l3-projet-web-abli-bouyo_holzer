<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:41
 */

namespace app\model;
use Illuminate\Database\Eloquent\Model;

class deck extends Model
{
    protected $table = 'deck';
    protected $primaryKey = "idDeck, idCarte";
    public $timestamps = false;
}