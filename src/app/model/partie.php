<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:46
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class partie extends Model
{
    protected $table = 'partie';
    protected $primaryKey = "id";
    public $timestamps = false;
}