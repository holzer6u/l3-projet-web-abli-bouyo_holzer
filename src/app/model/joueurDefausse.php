<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:44
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class joueurDefausse extends Model
{
    protected $table = 'joueurdefausse';
    protected $primaryKey = "idJoueur,idCarte";
    public $timestamps = false;
}