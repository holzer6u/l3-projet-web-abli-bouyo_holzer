<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:45
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class manche extends Model
{
    protected $table = 'manche';
    protected $primaryKey = "idManche";
    public $timestamps = false;
}