<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:43
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class historique extends Model
{
    protected $table = 'historique';
    protected $primaryKey = "idItem";
    public $timestamps = false;
}