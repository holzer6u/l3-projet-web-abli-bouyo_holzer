<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 10:45
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class joueurMain extends Model
{
    protected $table = 'joueurmain';
    protected $primaryKey = "idJoueur,idCarte";
    public $timestamps = false;
}