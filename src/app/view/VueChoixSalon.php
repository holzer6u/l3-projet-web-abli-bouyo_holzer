<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 06/12/2017
 * Time: 13:45
 */

namespace app\view;
use app\model\partie;

class VueChoixSalon
{


    public function render(){
        $this->content = $this->afficherSalons();
        $html = Vue::render($this->content);
        echo $html;
    }

    public function afficherSalons(){
        $app = \Slim\Slim::getInstance();
        //print_r($_SESSION["idJoueur"]);
        $content = <<<END
            <div class="salons">
END;
        $creerSalon = $app->urlFor('gamesAdd');
        $salons = partie::get();
        foreach ($salons as $s){
            $idSalon = $s["id"];
            $nbJoueur = $s["nbJoueur"];
            $idManche = $s["idManche"];
            $rejoindreGroupe = $app->urlFor('game',["id"=>$idSalon,"nbJoueur"=>$nbJoueur,"idManche"=>$idManche]);

            $content .= <<<END
                <div class="salon">
                    <div class="idSalon">
                        $idSalon
                    </div>
                    <div class="nbJoueur">
                        Max joueurs : $nbJoueur
                    </div>
                    <div class="bouttonRejoindre">
                        <form action="${rejoindreGroupe}" method="get">
                            <input type="submit" name="Rejoindre" value="Rejoindre" />
                        </form>
                    </div>
                </div>
END;
        }
        $content .= <<<END
            <div class="creation">
                <p> Créer une partie </p>
                <form action="${creerSalon}" method="post">
                    <label for='number'>Nombre de joueurs :</label>
                    <input type="number" name="nbJ" min="2" max="4">
                    <input type="submit" name="Valider" value="Valider" />
                </form>
            </div>
            </div>
END;

        return $content;
    }
}