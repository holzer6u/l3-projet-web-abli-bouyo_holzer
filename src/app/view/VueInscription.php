<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 07/12/2017
 * Time: 18:38
 */

namespace app\view;
use app\model\joueur;
use Slim\Slim;

class VueInscription
{
    public function render($a){
        $app = \Slim\Slim::getInstance();
        $validerInscription = $app->urlFor("validerInscription");
        $accueil = $app->urlFor("login");
        if($a == 2){
            $erreur = <<<END
            <div class="erreur"><p>Ce pseudo est déjà pris</p></div>
END;
        }else{
            $erreur = <<<END
END;
        }
        $content = <<<END
        <div class="cadreCon">
            $erreur
            <p> Inscrivez-vous simplement !</p>
            <form action="${validerInscription}" method="post"> 
                <label for='pseudo'>Pseudo : </label>
                <input type="text" name="pseudo" id="pseudo" required/> <br> <br>
                <label for="mdp">Mot de passe : </label>
                <input type="text" name="mdp" id="mdp" required/> <br> <br>
                <input type="submit" name="inscription" value="Inscription" />
            </form>    
            <br>
            <form action="${accueil}" method="get">
               <input type="submit" name="retour" value="Retour login" />
            </form>
        </div>
END;
        $html = Vue::render($content);
        echo $html;
    }

    
}