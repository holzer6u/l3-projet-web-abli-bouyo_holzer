<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 23/11/2017
 * Time: 11:10
 */

namespace app\view;


class Vue
{

    static function render($content){
        $app = \Slim\Slim::getInstance();
        $accueil = $app->urlFor('login');
        //$vueCarte = $app->urlFor('vueCarte');

        $html = <<<END
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset='utf-8'>
                <title>Love Letter</title>
                <link rel="stylesheet" href="${accueil}/web/css/stylesheet.css" />
                <script charset="utf-8" src="http://code.jquery.com/jquery-1.6.1.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <script src="${accueil}/web/js/js.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
                <link href="${accueil}/plugins/bootstrap/css/bootstrap.css" />
                <script src="${accueil}/plugins/bootstrap/js/bootstrap.min.js"></script>
            </head>
            <body>
                <div class="container">
                    $content
                </div>
                <div class="footer">
                     Alexis Abli-Bouyo / Benoît Holzer <br />
                     L3 Informatique - Loveletter 
                </div>
            </body>
        </html>
END;

        return $html;
    }

}