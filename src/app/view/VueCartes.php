<?php
namespace app\view;
use app\model\carte;
use app\controller\GameController;
use app\model\faceVisible;
use app\model\joueur;
use app\model\joueurMain;
use app\model\joueurDefausse;

class VueCartes
{
    private $content;
    private $listeCarte;

    public function __construct($c)
    {
        $this->content;
    }

    public function render(){
        $this->content = $this->afficherCartes();
        $html = Vue::render($this->content);
        echo $html;
    }

    private function afficherCartes()
    {
        $joueurs = joueur::where('idManche',$_SESSION['idManche'])->get();
        $app = \Slim\Slim::getInstance();
        $img = $app->urlFor('login');
        $scandaleChoix = $app->urlFor('choix');
        $scandaleAffichage = $app->urlFor('affichage');
        $scandaleJouer = $app->urlFor('jouer');
        $contenu = <<<END
  <div ><input id="aide_bouton" class="boutton_commande" type="submit" name="affichage" value="Aide sur les cartes">   </div>   
        <div  id="aide">

    <div id="aide_contenu">
        <div id="aide_haut"><button id="aide_croix">X</button></div>
        <p>
        8 - Princess 
(1), si vous défaussez cette carte, vous 
êtes éliminé de la manche.</p>
<p>
7 - Countess
 (1), Si vous avez cette carte en main en 
même temps que le King ou le Prince, alors vous 
devez défausser la carte de la Countess</p>
<p>
6  -  King
(1),  échangez  votre  main  avec  un  autre 
joueur de votre choix.</p>
<p>
5  -  Prince
 (2),  choisissez  un  joueur  (y  compris 
vous), celui-ci défausse la carte qu'il a en main pour 
en piocher une nouvelle.</p>
<p>
4 - Handmaid
 (2), Jusqu'au prochain tour, vous êtes 
protégé des effets des cartes des autres joueurs.</p>
<p>
3 - Baron
 (2) - Comparez votre carte avec celle d'un 
autre joueur, celui qui a la carte avec la plus faible 
valeur est éliminé de la manche.</p>
<p>
2 - Priest
 (2) - Regardez la main d'un autre joueur.</p>
<p>
1 - Guard
 (5) - Choisissez un joueur et essayez de 
deviner la carte qu'il a en main (excepté le Guard), 
si  vous  tombez  juste,  le  joueur  est  éliminé  de  la 
manche.</p>
<p></p>
    </div>

</div> 
<div id="revelerMain">
        <div id="revelerMain_haut"><button id="revelerMain_croix">X</button></div>

    <div id="revelerMain_contenu">
    
    </div>        
 
    

</div> 
<div id="selectionCarte">

    <p>Devinez la carte dans la main de votre adversaire (sauf un Guard) pour lui faire perdre la manche !</p>
    
    <img src='${img}/web/img/priest.png' onclick="loveletter.fonctions.selectionCarteFinir(2)">
    <img src='${img}/web/img/baron.png' onclick="loveletter.fonctions.selectionCarteFinir(3)">
    <img src='${img}/web/img/handmaid.png' onclick="loveletter.fonctions.selectionCarteFinir(4)">
    <img src='${img}/web/img/prince.png' onclick="loveletter.fonctions.selectionCarteFinir(5)">
    <img src='${img}/web/img/king.png' onclick="loveletter.fonctions.selectionCarteFinir(6)">
    <img src='${img}/web/img/countess.png' onclick="loveletter.fonctions.selectionCarteFinir(7)">
    <img src='${img}/web/img/princess.png' onclick="loveletter.fonctions.selectionCarteFinir(8)">
            
 
    

</div> 

<div  id="selectionJoueur">

    <div id="selectionJoueur_contenu">
    
    </div>
<!--<div id="selectionJoueur_bas"><button id="selectionJoueur_ok">OK</button></div>-->
</div>

<script> var scandaleChoix = "$scandaleChoix";
var scandaleAffichage = "$scandaleAffichage";
var scandaleJouer = "$scandaleJouer";</script>

        <div class="joueurs">    


END;
        $indice = 1;
        foreach ($joueurs as $joueur){

            $joueurId = $joueur['id'];
            $joueurNom = joueur::where('id',$joueurId)->first();
            $joueurNom = $joueurNom['login'];
            $carteJoueurMain = joueurMain::where('idJoueur',$joueur['id'])->get();
            $contenu .= <<<END
                <div class='joueur'>
                    <p> $joueurNom :</p>
                    <div class='cartes' id="cartesJ$indice">
                        
END;
            $indice++;
            $idJoueur = $_SESSION["idJoueur"];
            foreach ($carteJoueurMain as $cJ){
                $carte = carte::where('idCarte',$cJ['idCarte'])->first();
                $nomImg = $carte['nom'];
                $carteId = $carte['idCarte'];
                if($joueurId == $_SESSION['idJoueur']) {
                    $lienImg = $carte['img'];
                    // .piocher()
                    $contenu .= <<<END
                    <img src='${img}/web/$lienImg' alt="" onmouseover="glow(this,true)" onmouseout="glow(this,false)">


END;
                } else {
                    $contenu .= <<<END
                        <img src='${img}/web/img/dos.png'>
END;
                }

            }
            $contenu .= <<<END
                    </div>
                </div>
            
                
END;
        }

        $contenu .= <<<END
            </div>
            <div class="carteBrulees">
                <div class='cartes' id="cartesBrulay">
END;

        $cartesBrulees = faceVisible::where('idManche',$_SESSION['idManche'])->get();
        foreach ($cartesBrulees as $cb){
            $idCarte = $cb['idCarte'];
            $carte = carte::select('nom','img')->where('idCarte',$idCarte)->first();
            $nomCarte = $carte['nom'];
            $lienImg = $carte['img'];
            $contenu .= <<<END
                <img src='${img}/web/$lienImg' alt="">
                
END;
        }
        $contenu .= <<<END
                </div>
            </div>
            <div class="cartesJouees">
END;
        foreach ($joueurs as $joueur){
            $joueurId = $joueur['id'];
            $joueurNom = joueur::where('id',$joueurId)->first();
            $joueurNom = $joueurNom['login'];
            $carteJoueurDefausse = joueurDefausse::where('idJoueur',$joueur['id'])->get();
            $contenu .= <<<END
                <div class='joueurCarteJouees'>
                    <p> Cartes jouées par $joueurNom :</p>
                    <div class='cartes' id="cartesJoueesJ$joueurId">
END;
            foreach ($carteJoueurDefausse as $cJ) {
                $carte = carte::where('idCarte', $cJ['idCarte'])->first();
                $lienImg = $carte['img'];
                $contenu .= <<<END
                    <img src='${img}/web/$lienImg' alt="">
END;
            }
            $contenu .= <<<END
                </div>
            </div>
END;
        }

        $contenu .= <<<END
        
   
        <div id="historique">
            <p></p>
            
        </div>
        </div>
END;
        return $contenu;
    }
}