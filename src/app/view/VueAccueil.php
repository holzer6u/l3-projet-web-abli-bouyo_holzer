<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 03/12/2017
 * Time: 12:36
 */

namespace app\view;
use app\view\Vue;


class VueAccueil
{
    public function render($a)
    {
        $app = \Slim\Slim::getInstance();
        $ins = $app->urlFor('inscription');
        if($a == 2){
            $erreur = <<<END
            <div class="erreur"><p>Informations incorrectes</p></div>
END;
        }else{
            $erreur = <<<END
END;
        }
        $validerConnexion = $app->urlFor('verifierConnexion');
        $this->content = <<<END
        <div class="cadreCon">
            $erreur
            <form action="${validerConnexion}" method="post"> 
            <label for='pseudo'>Pseudo : </label>
            <input type="text" name="pseudo" id="pseudo" required/> <br> <br>
            <label for="mdp">Mot de passe : </label>
            <input type="password" name="mdp" id="mdp" required/> <br> <br>
     
            <input type="submit" name="connexion" value="Connexion" />
            </form>   
            <br>
            <a href="${ins}">S'inscrire</a>
        </div>
END;
        $html = Vue::render($this->content);
        echo $html;
    }

}