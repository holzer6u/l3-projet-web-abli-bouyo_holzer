/**
 * Created by Benoît on 07/12/2017.
 */

function glow(img, etat){
    if(etat) {
        img.style.boxShadow = "0px 0px 25px 2px #cf5";
    }else if(!img.classList.contains('selected')){
        img.style.boxShadow = "0px 0px 0px 0px #cf5";
    }
}

/*
function tirerCarte(joueur){
    $.ajax({

        url: "/l3-projet-web-abli-bouyo_holzer/index.php/draw",
        type: 'get',
        dataType: 'html',
        success: function(data){
            alert("ok");
            console.log(data);
        }
    });
}
*/

var loveletter = (function () {
    return{
        fonctions: {}
    };
})();



loveletter.fonctions = (function () {

    return{
        init: function () {
            $('#aide_bouton').click(loveletter.fonctions.aideOuvrir);
            $("#aide_croix").click(loveletter.fonctions.aideFermer);
            $("#revelerMain_croix").click(loveletter.fonctions.revelerMainFermer);
            setInterval(loveletter.fonctions.affichage, 1000);
        },

        aideOuvrir: function () {
            $("#aide").css('display','block');
        },
        aideFermer: function () {
             $("#aide").css('display','none');
        },

        revelerMainOuvrir: function (text) {
            //alert(text);

            var tab = text.split("|");
            var i = 1;
            $("#revelerMain_contenu").children().remove();
            var p = $('<p>').text("Main de "+tab[i]+": ");
            $("#revelerMain_contenu").append(p);
            i++;

            while(tab[i]!="finRev"){
                var img = $('<img>').attr('data-img',tab[i]).attr('src',tab[i]);
                $("#revelerMain_contenu").append(img);

                i++;
            }


            $("#revelerMain").css('display','block');
        },
        revelerMainFermer: function () {
            $("#revelerMain").css('display','none');
        },

        selectionJoueurOuvrir: function (text) {
            //alert(text);
            var tab = text.split("|");
            var i = 1;
            $("#selectionJoueur_contenu").children().remove();
            var p = $('<p>').text("Choisissez un joueur: ");
            $("#selectionJoueur_contenu").append(p);

            while(tab[i]!="finSel"){
                var id = tab[i];
                i++;
                var a = $('<a>').text(tab[i]).attr('onclick','loveletter.fonctions.selectionJoueurFinir('+id+')');
                p = $('<p>').append(a);
                $("#selectionJoueur_contenu").append(p);

                i++;
            }

            $("#selectionJoueur").css('display','block');
        },
        selectionJoueurFermer: function () {
            $("#selectionJoueur").css('display','none');
        },
        selectionJoueurFinir: function (num) {
            //loveletter.fonctions.selectionJoueurFermer;
            $("#selectionJoueur").css('display','none');
            loveletter.fonctions.choix(num);

        },
        selectionCarteOuvrir: function () {

            $("#selectionCarte").css('display','block');
        },
        selectionCarteFermer: function () {
            $("#selectionCarte").css('display','none');
        },
        selectionCarteFinir: function (num) {
            //loveletter.fonctions.selectionJoueurFermer;
            $("#selectionCarte").css('display','none');
            loveletter.fonctions.choix(num);

        },

        jouer: function (num){
            $.ajax({

                url: (scandaleJouer.slice(0,-3)+"/"+num),
                type: 'post',
                dataType: 'text',
                success: function(text, statut){
                    //alert(statut+" "+text);
                   // console.log(text);
                    loveletter.fonctions.affichage();
                    if(text!='ok'){
                        var tab = text.split("|");
                        switch(tab[0]){
                            case "selection":
                                loveletter.fonctions.selectionJoueurOuvrir(text);
                                break;
                            case "revelerMain":
                                loveletter.fonctions.revelerMainOuvrir(text);
                                break;
                        }

                    }

                },
                error : function(resultat, statut, erreur){
                    alert(statut+" "+ resultat+" "+erreur);
                    console.log(erreur);
                }
            });
        },
        choix: function (num){
            $.ajax({

                url: (scandaleChoix.slice(0,-3)+"/"+num),
                type: 'post',
                dataType: 'text',
                success: function(text, statut){
                    //alert(statut+" "+text);
                    if(text!='choixOk'){
                        var tab = text.split("|");
                        switch(tab[0]){
                            case "revelerMain":
                                loveletter.fonctions.revelerMainOuvrir(text);
                                break;
                            case "choixCarte":
                                loveletter.fonctions.selectionCarteOuvrir();
                                break;
                        }

                    }
                    //console.log(text);
                    //loveletter.fonctions.affichage();
                },
                error : function(resultat, statut, erreur){
                    alert(statut+" "+ resultat+" "+erreur);

                }
            });
        },
        affichage: function (){
            $.ajax({

                url: scandaleAffichage,
                type: 'post',
                dataType: 'text',
                success: function(text, statut){
                    //alert(statut+" "+text);
                    console.log(text);
                    var tab = text.split("|");
                    //console.log(tab);
                    var i = 0;
                    var j=1;
                    var ctoi, ctontour, countess;
                    var id;


                    while(tab[i]!="finCM"){
                        str = "#cartesJ" + j;
                        $(str).children().remove();
                        ctoi = false;
                        ctontour = false;
                        if(tab[i]=="CTOI"){
                            ctoi = true;
                            i++;
                            if(tab[i]=="CTONTOUR"){
                                ctontour = true;
                                i++;
                            }

                            if(tab[i]=="CTONTOURmaisCountess"){
                                countess = true;
                                i++;
                            }
                        }
                        while(tab[i]!="finJ"){
                            if(ctoi){
                                id=tab[i];
                                i++;
                            }
                            var img = $('<img>').attr('data-img',tab[i]).attr('src',tab[i]);
                            if(ctoi){

                                if(ctontour){
                                    img.attr('onmouseover','glow(this,true)')
                                        .attr('onmouseout','glow(this,false)');
                                    img.attr('onclick','loveletter.fonctions.jouer('+id+')');
                                }
                                if(countess && id == 15){
                                    img.attr('onmouseover','glow(this,true)')
                                        .attr('onmouseout','glow(this,false)');
                                    img.attr('onclick','loveletter.fonctions.jouer('+id+')');
                                }

                            }
                            $(str).append(img);

                            i++;
                        }
                        j++;
                        i++;
                    }
                    i++;
                    $("#cartesBrulay").children().remove();
                    while(tab[i]!="finCB"){


                            var img = $('<img>').attr('data-img',tab[i]).attr('src',tab[i]);
                            $("#cartesBrulay").append(img);

                            i++;
                    }
                    i++;
                    j=1;

                    while(tab[i]!="finCD"){
                        str = "#cartesJoueesJ" + j;
                        $(str).children().remove();
                        while(tab[i]!="finJD"){
                            var img = $('<img>').attr('data-img',tab[i]).attr('src',tab[i]);

                            $(str).append(img);

                            i++;
                        }
                        j++;
                        i++;
                    }

                    i++;
                    $("#historique").children().remove();
                    while(tab[i]!="finH"){


                        var p = $('<p>').text(tab[i]);
                        $("#historique").append(p);

                        i++;
                    }
                //alert(i);




                },
                error : function(resultat, statut, erreur){
                    alert(statut+" "+ resultat+" "+erreur);

                }
            });
        }
    };
})();

$(document).ready(function() {
    if (typeof scandaleChoix === 'undefined') {

    }else{
        loveletter.fonctions.init();
    }


});


