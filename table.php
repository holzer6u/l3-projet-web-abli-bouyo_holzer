<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <title>Love Letter</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="jeuTable.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/cartes.png">

</head>
<body>
<div id="table">

    <article id="gauche">
        <section id="joueur3">
            <img id="j31" class="carte" src="images/cartes/vide.png">
            <img id="j32" class="carte" src="images/cartes/vide.png">
            <p id="nomjoueur3">joueur 3</p>
        </section>

    </article>

    <article id="milieu">
        <section id="joueur2">
            <img id="j21" class="carte" src="images/cartes/vide.png">
            <img id="j22" class="carte" src="images/cartes/vide.png">
            <p id="nomjoueur2">joueur 2</p>
        </section>

        <section id="cartecentral">
            <img id="c1" class="carte" src="images/cartes/vide.png">
            <img id="c2" class="carte" src="images/cartes/vide.png">
            <img id="c3" class="carte" src="images/cartes/vide.png">
            <img id="c4" class="carte" src="images/cartes/vide.png">
            <img id="c5" class="carte" src="images/cartes/vide.png">
        </section>

        <section id="joueur1">
            <img id="j11" class="carte" src="images/cartes/vide.png">
            <img id="j12" class="carte" src="images/cartes/vide.png">
            <p id="nomjoueur1">joueur 1</p>
        </section>

    </article>
    <article id="milieu">
    <section id="joueur4">
        <img id="j41" class="carte" src="images/cartes/vide.png">
        <img id="j42" class="carte" src="images/cartes/vide.png">
        <p id="nomjoueur4">joueur 4</p>
    </section>

    </article>






</div>

<div id="commandes">
    <input id="selever" class="boutton_commande" type="submit" name="selever" value="Quitter la table">
    <input id="secoucher" class="boutton_commande" type="submit" name="secoucher" value="Se coucher">
    <input id="suivre" class="boutton_commande" type="submit" name="suivre" value="Suivre">
    <input id="relancer" class="boutton_commande" type="submit" name="relancer" value="Relancer" >
    <input id="valeur" type="range" name="valeur" value="0" min="0" max="300" >
    <label id="mise">Mise: 0</label>
</div>

<footer>
    <hr>
</footer>


</body>
</html>
