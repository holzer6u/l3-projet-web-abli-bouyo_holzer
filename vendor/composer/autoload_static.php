<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite090bde6992cce2cb667fab05e6201a0
{
    public static $files = array (
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '3b5531f8bb4716e1b6014ad7e734f545' => __DIR__ . '/..' . '/illuminate/support/Illuminate/Support/helpers.php',
        'e40631d46120a9c38ea139981f8dab26' => __DIR__ . '/..' . '/ircmaxell/password-compat/lib/password.php',
    );

    public static $prefixLengthsPsr4 = array (
        'a' => 
        array (
            'acurrieclark\\PhpPasswordVerifier\\' => 33,
        ),
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Component\\Translation\\' => 30,
        ),
        'C' => 
        array (
            'Carbon\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'acurrieclark\\PhpPasswordVerifier\\' => 
        array (
            0 => __DIR__ . '/..' . '/acurrieclark/php-password-verifier/src',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Component\\Translation\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/translation',
        ),
        'Carbon\\' => 
        array (
            0 => __DIR__ . '/..' . '/nesbot/carbon/src/Carbon',
        ),
    );

    public static $prefixesPsr0 = array (
        'a' => 
        array (
            'app' => 
            array (
                0 => __DIR__ . '/../..' . '/src',
            ),
        ),
        'S' => 
        array (
            'Slim' => 
            array (
                0 => __DIR__ . '/..' . '/slim/slim',
            ),
        ),
        'P' => 
        array (
            'PasswordPolicy' => 
            array (
                0 => __DIR__ . '/../..' . '/lib',
            ),
        ),
        'I' => 
        array (
            'Illuminate\\Support' => 
            array (
                0 => __DIR__ . '/..' . '/illuminate/support',
            ),
            'Illuminate\\Events' => 
            array (
                0 => __DIR__ . '/..' . '/illuminate/events',
            ),
            'Illuminate\\Database' => 
            array (
                0 => __DIR__ . '/..' . '/illuminate/database',
            ),
            'Illuminate\\Container' => 
            array (
                0 => __DIR__ . '/..' . '/illuminate/container',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite090bde6992cce2cb667fab05e6201a0::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite090bde6992cce2cb667fab05e6201a0::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInite090bde6992cce2cb667fab05e6201a0::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
