-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 07 Décembre 2017 à 19:18
-- Version du serveur :  10.1.16-MariaDB
-- Version de PHP :  5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `loveletter`
--

-- --------------------------------------------------------

--
-- Structure de la table `carte`
--

CREATE TABLE `carte` (
  `idCarte` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `img` varchar(80) NOT NULL,
  `valeur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `carte`
--

INSERT INTO `carte` (`idCarte`, `nom`, `img`, `valeur`) VALUES
(1, 'Guard', '/img/guard.png', 1),
(2, 'Guard', '/img/guard.png', 1),
(3, 'Guard', '/img/guard.png', 1),
(4, 'Guard', '/img/guard.png', 1),
(5, 'Guard', '/img/guard.png', 1),
(6, 'Priest', '/img/priest.png', 2),
(7, 'Priest', '/img/priest.png', 2),
(8, 'Baron', '/img/baron.png', 3),
(9, 'Baron', '/img/baron.png', 3),
(10, 'Handmaid', '/img/handmaid.png', 4),
(11, 'Handmaid', '/img/handmaid.png', 4),
(12, 'Prince', '/img/prince.png', 5),
(13, 'Prince', '/img/prince.png', 5),
(14, 'King', '/img/king.png', 6),
(15, 'Countess', '/img/countess.png', 7),
(16, 'Princess', '/img/princess.png', 8);

-- --------------------------------------------------------

--
-- Structure de la table `deck`
--

CREATE TABLE `deck` (
  `idDeck` int(11) NOT NULL,
  `idCarte` int(11) NOT NULL,
  `idManche` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `facevisible`
--

CREATE TABLE `facevisible` (
  `idCarte` int(11) NOT NULL,
  `idManche` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--


CREATE TABLE `joueur` (
  `login` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `idManche` int(11) NOT NULL,
  `protege` int(1) NOT NULL,
  `ordre` int(1) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `joueur`
--

INSERT INTO `joueur` (`login`, `mdp`, `id`, `score`, `idManche`, `protege`, `ordre`) VALUES
('Absolitaire', '123456', 1, 0, 0, 0, 0 ),
('Benhol', '654321', 2, 0, 22, 0, 0),
('a', 'sdqsdq', 6, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `joueurdefausse`
--

CREATE TABLE `joueurdefausse` (
  `idJoueur` int(11) NOT NULL,
  `idCarte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `joueurmain`
--

CREATE TABLE `joueurmain` (
  `idJoueur` int(11) NOT NULL,
  `idCarte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `manche`
--

CREATE TABLE `manche` (
  `idManche` int(11) NOT NULL,
  `idDeck` int(11) DEFAULT NULL,
  `enCours` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE `partie` (
  `id` int(11) NOT NULL,
  `nbJoueur` int(11) NOT NULL,
  `idManche` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `carte`
--
ALTER TABLE `carte`
  ADD PRIMARY KEY (`idCarte`);

--
-- Index pour la table `deck`
--
ALTER TABLE `deck`
  ADD PRIMARY KEY (`idDeck`,`idCarte`);

--
-- Index pour la table `facevisible`
--
ALTER TABLE `facevisible`
  ADD PRIMARY KEY (`idCarte`,`idManche`);

--
-- Index pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `joueurdefausse`
--
ALTER TABLE `joueurdefausse`
  ADD PRIMARY KEY (`idJoueur`,`idCarte`);

--
-- Index pour la table `joueurmain`
--
ALTER TABLE `joueurmain`
  ADD PRIMARY KEY (`idJoueur`,`idCarte`);

--
-- Index pour la table `manche`
--
ALTER TABLE `manche`
  ADD PRIMARY KEY (`idManche`);

--
-- Index pour la table `partie`
--
ALTER TABLE `partie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `joueur`
--
ALTER TABLE `joueur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `manche`
--
ALTER TABLE `manche`
  MODIFY `idManche` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pour la table `partie`
--
ALTER TABLE `partie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- --------------------------------------------------------

--
-- Structure de la table `historique`
--

CREATE TABLE `historique` (
  `idItem` int(11) DEFAULT NULL,
  `idManche` int(11) NOT NULL,
  `contenu` varchar(150) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour la table `historique`
--
ALTER TABLE `historique`
  ADD PRIMARY KEY (`idItem`);

--
-- AUTO_INCREMENT pour la table `historique`
--
ALTER TABLE `historique`
  MODIFY `idItem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;





